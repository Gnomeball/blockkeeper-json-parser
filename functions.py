# Class imports
from prettytable import PrettyTable
from session import Session
from puzzle import Puzzle

# Static imports
from statistics import mean
from math import floor

# General imports
import matplotlib.pyplot as plt
import argparse
import json

def parseArgs() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description = "A Command Line tool for parsing the JSON data from BlockKeeper into a more reasonably sized chart.",
        epilog = "Flags 's', 'a' and 'c' can be used at the same time as each other to get multiple outputs."
    )

    parser.add_argument("-f", dest = "file", required = True, help = "Which file to parse")
    parser.add_argument("-p", dest = "puzzle", help = "Which puzzle to chart")
    parser.add_argument("-s", dest = "sessions", action = "store_true", help = "To output puzzle names")
    parser.add_argument("-a", dest = "averages", action = "store_true", help = "To output averages")
    parser.add_argument("-c", dest = "chart", action = "store_true", help = "To render a chart")

    return parser.parse_args()

def parseFile(filename: str) -> {Puzzle}:
    puzzles = {}
    with open(filename) as file:
        data = json.load(file)
        p_data = data["puzzles"]
        for p in p_data:
            sessions = [Session(s["date"], s["name"], s["records"]) for s in p["sessions"]]
            puzzles.__setitem__(p["name"], Puzzle(p["name"], sessions))
    return puzzles

def __average(times :[int]) -> int:
    drop = floor(len(times) * 0.05) if len(times) > 25 else 1
    times.sort()
    average = mean(times[drop:len(times) - drop])
    return average

def calculateAverages(times: [int]) -> [(int, [int])]:
    allAverageSizes = [5, 12, 25, 50, 100, 500, 1000, 5000, 10000]
    averageSizes = [size for size in allAverageSizes if size < len(times)]
    averages = []

    for s in averageSizes:
        a = [None] * s
        for t in range(s, len(times)):
            a.append(__average(times[t - s:t]))
        averages.append((s, a))

    return averages

def printSessions(puzzles: {Session}) -> None:
    for session in puzzles:
        sessions = puzzles[session].sessions
        if len(sessions) != 0:
            print(f"{str(session)}: ")
            [print(f" - {s.name}") for s in sessions].append(print())

def printAverages(puzzle: str, averages: [int]) -> None:
    table = PrettyTable(["Avg", "Best", "Current"])
    table.title = puzzle
    table.align = "l"

    for a in averages:
        table.add_row([
            f"Ao{str(a[0])}",
            str(format(min(a[1][a[0]:]), ".3f")),
            str(format(a[1][len(a[1])-1], ".3f"))
        ])

    print(table)

def makeChart(puzzle: str, times: [int]) -> None:

    plt.ylim(min(times) * 0.98, max(times) * 1.02)
    plt.grid(True, "both", "y")
    plt.title(f"{puzzle} Times")
    plt.ylabel("Time (s)")
    plt.xlabel("Solve N.")

    ids = range(0, len(times))
    plt.plot(ids, times, label = "Single")

    averages = calculateAverages(times)

    for a in averages:
        plt.plot(ids, a[1], label = f"Ao{str(a[0])}")

    plt.legend()

    plt.show()

def getTimes(sessions: []):
    times = []
    for session in sessions:
        for record in session.records:
            if record["result"] == "OK":
                times.append(record["time"])
    return times