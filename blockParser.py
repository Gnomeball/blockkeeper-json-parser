from functions import (
    parseArgs, parseFile,
    getTimes, calculateAverages,
    printAverages, printSessions,
    makeChart
)

args = parseArgs()
puzzles = parseFile(args.file)

if args.sessions:
    printSessions(puzzles)

if args.puzzle == None:
    exit("No puzzle selected, exiting.")

try:
    puzzle = puzzles[args.puzzle]
except KeyError:
    exit(f"Puzzle \'{args.puzzle}\' not found.")

sessions = puzzle.sessions

if len(sessions) == 1:
    times = getTimes(sessions)
elif len(sessions) > 1:
    print(f"More than one session found for {args.puzzle}.")
    [print(f" - {i}: {sessions[i].name}") for i in range(0, len(sessions))]
    n = ""
    while n not in [str(n) for n in list(range(0, len(sessions)))]:
        n = input("Please select a session: ")
    print(f"Using session: {sessions[int(n)].name}")
    times = getTimes([sessions[int(n)]])

if len(times) == 0:
    exit(f"No times found for {args.puzzle}.")

if args.averages:
    printAverages(args.puzzle, calculateAverages(times))

if args.chart:
    makeChart(args.puzzle, times)
