class Session:

    def __init__(self, date: str, name: str, records: list):
        self.date = date
        self.name = name
        self.records = records