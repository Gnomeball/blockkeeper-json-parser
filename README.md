# BlockParser

A Command Line tool for parsing the JSON data from BlockKeeper into a more reasonably sized chart.

```
optional arguments:
  -h, --help  show this help message and exit
  -f FILE     Which file to parse
  -p PUZZLE   Which puzzle to chart
  -s          To output puzzle names
  -a          To output averages
  -c          To render a chart
```

Flags 's', 'a' and 'c' can be used at the same time as each other to get multiple outputs.

## Installation

This tool requires the following Python Libraries to be installed:

```
json
argparse
matlibplot
statistics
prettytable
```

## Output

Invoking the `-a` flag will print out basic session stats for a puzzle.

```
$ python blockParser.py -a -p 3x3x3 -f puzzles.json
+-------+--------+---------+
| 3x3x3 | Best   | Current |
+-------+--------+---------+
| Ao5   | 16.894 | 19.931  |
| Ao12  | 17.635 | 19.410  |
| Ao25  | 18.475 | 19.632  |
| Ao50  | 19.154 | 19.462  |
| Ao100 | 19.542 | 19.629  |
+-------+--------+---------+
```

Invoking the `-s` flag will simply output session names.

```
$ python blockParser.py -s -p 3x3x3 -f puzzles.json
2x2x2
3x3x3
4x4x4

... etc ...

3x3x3 FT
234567 Relay
Other
```

Invoking the `-c` command will render a chart with plot lines for single solves as well as averages of 5, 12, 25, 50, 100, 500, 1000, 5000, 10000 (if available).

```
$ python blockParser.py -c -p 3x3x3 -f puzzles.json
```

Multiple flags may be used at once for multiple outputs.

## Screenshots

![Sample 3x3 Output](./Sample3x3x3Output.png "Sample 3x3 Output")